package com.ristekmuslim.travelristek.airport;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by imamudin on 21/08/17.
 */

public class AirportModel {
    private String C_CODE        = "";
    private String C_NAME        = "";
    private String C_CITYCODE    = "";
    private String C_CITYNAME    = "";
    private String C_COUNTRYNAME = "";
    private String C_COUNTRYCODE = "";
    private String C_TIMEZONE    = "";
    private String C_LAT         = "";
    private String C_LON         = "";
    private String C_NUMAIRPORT  = "";
    private String C_CITY        = "";

    public String dataJson;
    
    public AirportModel() {
    }

    public String getC_CODE() {
        return C_CODE;
    }

    public void setC_CODE(String c_CODE) {
        C_CODE = c_CODE;
    }

    public String getC_NAME() {
        return C_NAME;
    }

    public void setC_NAME(String c_NAME) {
        C_NAME = c_NAME;
    }

    public String getC_CITYCODE() {
        return C_CITYCODE;
    }

    public void setC_CITYCODE(String c_CITYCODE) {
        C_CITYCODE = c_CITYCODE;
    }

    public String getC_CITYNAME() {
        return C_CITYNAME;
    }

    public void setC_CITYNAME(String c_CITYNAME) {
        C_CITYNAME = c_CITYNAME;
    }

    public String getC_COUNTRYNAME() {
        return C_COUNTRYNAME;
    }

    public void setC_COUNTRYNAME(String c_COUNTRYNAME) {
        C_COUNTRYNAME = c_COUNTRYNAME;
    }

    public String getC_COUNTRYCODE() {
        return C_COUNTRYCODE;
    }

    public void setC_COUNTRYCODE(String c_COUNTRYCODE) {
        C_COUNTRYCODE = c_COUNTRYCODE;
    }

    public String getC_TIMEZONE() {
        return C_TIMEZONE;
    }

    public void setC_TIMEZONE(String c_TIMEZONE) {
        C_TIMEZONE = c_TIMEZONE;
    }

    public String getC_LAT() {
        return C_LAT;
    }

    public void setC_LAT(String c_LAT) {
        C_LAT = c_LAT;
    }

    public String getC_LON() {
        return C_LON;
    }

    public void setC_LON(String c_LON) {
        C_LON = c_LON;
    }

    public String getC_NUMAIRPORT() {
        return C_NUMAIRPORT;
    }

    public void setC_NUMAIRPORT(String c_NUMAIRPORT) {
        C_NUMAIRPORT = c_NUMAIRPORT;
    }

    public String getC_CITY() {
        return C_CITY;
    }

    public void setC_CITY(String c_CITY) {
        C_CITY = c_CITY;
    }

    public AirportModel(String objectPoin){
        this.dataJson   = objectPoin;
        try{
            JSONObject poin = new JSONObject(objectPoin);

        }catch (Exception e){
            Log.d(getClass().toString(),"error memproses data object.");
        }

    }
}