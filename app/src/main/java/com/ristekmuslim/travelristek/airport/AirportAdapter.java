package com.ristekmuslim.travelristek.airport;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.ristekmuslim.travelristek.R;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by imamudin on 20/08/17.
 */

public class AirportAdapter extends BaseAdapter implements
        StickyListHeadersAdapter, SectionIndexer {

    private int[] mSectionIndices;
    private Character[] mSectionLetters;
    private LayoutInflater mInflater;

    private List<AirportModel> airports = null;

    public AirportAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        //mCountries = context.getResources().getStringArray(R.array.countries);
        //addAirport();
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
    }
    public AirportAdapter(Context context, List<AirportModel> airports) {
        mInflater = LayoutInflater.from(context);
        //mCountries = context.getResources().getStringArray(R.array.countries);
        this.airports = airports;
        Log.d("sdfds","total di adapter: "+airports.size());
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
    }

    private int[] getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        String countryHeader = airports.get(0).getC_CITYNAME();
        sectionIndices.add(0);
        for (int i = 1; i < airports.size(); i++) {
            if (!airports.get(i).getC_COUNTRYNAME().equals(countryHeader)) {
                Log.d("ket :  ",airports.get(i).getC_CITYNAME()+"  ==  "+countryHeader);
                countryHeader = airports.get(i).getC_CITYNAME();
                sectionIndices.add(i);
            }
        }
        Log.d("total ","total header = "+sectionIndices.size());
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }

        return sections;
    }

    private Character[] getSectionLetters() {
        Character[] letters = new Character[airports.size()];
        for (int i = 0; i < mSectionIndices.length; i++) {
            letters[i] = airports.get(mSectionIndices[i]).getC_CITYNAME().charAt(0);
        }
        return letters;
    }
    private String[] getSectionLettersString(){
        String[] letters = new String[airports.size()];
        Log.d("total ","total header  lagi = "+mSectionIndices.length);
        for (int i = 0; i < mSectionIndices.length; i++) {
            letters[i] = airports.get(i).getC_CITYNAME();
            Log.d("header ",i+"  = "+airports.get(mSectionIndices[i]).getC_CITYNAME());
        }
        return letters;
    }

    @Override
    public int getCount() {
        return airports.size();
    }

    @Override
    public Object getItem(int position) {
        return airports.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.airport_list_item_layout, parent, false);
            holder.tv_city = (TextView) convertView.findViewById(R.id.tv_city);
            holder.tv_airport = (TextView) convertView.findViewById(R.id.tv_airport);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_city.setText(airports.get(position).getC_CITYNAME());
        holder.tv_airport.setText(airports.get(position).getC_CODE()+" - "+airports.get(position).getC_NAME());

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;

        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = mInflater.inflate(R.layout.airport_header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        // set header text as first char in name
        CharSequence headerChar = airports.get(position).getC_CITYNAME().substring(0,1);
        holder.text.setText(headerChar);

        return convertView;
    }

    /**
     * Remember that these have to be static, postion=1 should always return
     * the same Id that is.
     */
    @Override
    public long getHeaderId(int position) {
        // return the first character of the country as ID because this is what
        // headers are based upon
        //return mCountries[position].subSequence(0, 1).charAt(0);
        return airports.get(position).getC_CITYNAME().subSequence(0, 1).charAt(0);
    }

    @Override
    public int getPositionForSection(int section) {
        if (mSectionIndices.length == 0) {
            return 0;
        }

        if (section >= mSectionIndices.length) {
            section = mSectionIndices.length - 1;
        } else if (section < 0) {
            section = 0;
        }
        return mSectionIndices[section];
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < mSectionIndices.length; i++) {
            if (position < mSectionIndices[i]) {
                return i - 1;
            }
        }
        return mSectionIndices.length - 1;
    }

    @Override
    public Object[] getSections() {
        return mSectionLetters;
    }

    public void clear() {
        mSectionIndices = new int[0];
        mSectionLetters = new Character[0];
        notifyDataSetChanged();
    }

    public void restore() {
        //mCountries = mContext.getResources().getStringArray(R.array.countries);
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
        notifyDataSetChanged();
    }

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {
        TextView tv_city;
        TextView tv_airport;
    }

}
