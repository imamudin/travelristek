package com.ristekmuslim.travelristek;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.ristekmuslim.travelristek.airport.ChooseAirport;
import com.ristekmuslim.travelristek.flight.ListFlight;
import com.ristekmuslim.travelristek.promo.SamplePromoAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by imamudin on 13/08/17.
 */

public class MainActivity extends AppCompatActivity {
    LinearLayout ll_origin, ll_destination, ll_depart_date, ll_return_date, ll_passenger, ll_seat_class, ll_search;
    ImageButton imBtn_swap_origin;
    Switch sw_round_trip;
    TextView tv_origin, tv_destination, tv_depart_date, tv_return_date, tv_passenger, tv_seat_class;
    Calendar c_depart, c_return;
    Button btn_search;

    //for passenger
    ImageButton imBtn_adult_min, imBtn_child_min, imBtn_infant_min, imBtn_adult_plu, imBtn_child_plu, imBtn_infant_plu;
    TextView tv_adult_total, tv_child_total, tv_infant_total;
    int adult_total=1, child_total=0, infant_total=0;

    //for seat class
    CheckBox cb_economy, cb_bussiness, cb_premium;
    boolean b_sc_economy=true, b_sc_bussiness=true, b_sc_premium=true;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flight);


        //untuk pager
        ViewPager viewpager = (ViewPager) findViewById(R.id.viewpager);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewpager.setAdapter(new SamplePromoAdapter());
        indicator.setViewPager(viewpager);


        ll_origin       = (LinearLayout)findViewById(R.id.ll_origins);
        ll_destination  = (LinearLayout)findViewById(R.id.ll_destination);
        ll_depart_date  = (LinearLayout)findViewById(R.id.ll_depart_date);
        ll_return_date  = (LinearLayout)findViewById(R.id.ll_return_date);
        ll_passenger    = (LinearLayout)findViewById(R.id.ll_passenger);
        ll_seat_class   = (LinearLayout)findViewById(R.id.ll_seat_class);
        ll_search       = (LinearLayout)findViewById(R.id.ll_search);

        ll_origin.setOnClickListener(btnClick);
        ll_destination.setOnClickListener(btnClick);
        ll_depart_date.setOnClickListener(btnClick);
        ll_return_date.setOnClickListener(btnClick);
        ll_passenger.setOnClickListener(btnClick);
        ll_seat_class.setOnClickListener(btnClick);
        ll_seat_class.setOnClickListener(btnClick);

        imBtn_swap_origin   = (ImageButton)findViewById(R.id.imBtn_swap_origin);
        imBtn_swap_origin.setOnClickListener(btnClick);

        btn_search      = (Button) findViewById(R.id.btn_search);
        btn_search.setOnClickListener(btnClick);

        tv_origin       = (TextView)findViewById(R.id.tv_origin);
        tv_destination  = (TextView)findViewById(R.id.tv_destination);
        tv_depart_date  = (TextView)findViewById(R.id.tv_depart_date);
        tv_return_date  = (TextView)findViewById(R.id.tv_return_date);
        tv_passenger    = (TextView)findViewById(R.id.tv_passenger);
        tv_seat_class   = (TextView)findViewById(R.id.tv_seat_class);
        
        //for passenger
        imBtn_adult_min = (ImageButton) findViewById(R.id.imBtn_adult_minus);
        imBtn_child_min = (ImageButton) findViewById(R.id.imBtn_child_minus);
        imBtn_infant_min = (ImageButton) findViewById(R.id.imBtn_infant_minus);
        imBtn_adult_plu = (ImageButton) findViewById(R.id.imBtn_adult_plus);
        imBtn_child_plu = (ImageButton) findViewById(R.id.imBtn_child_plus);
        imBtn_infant_plu = (ImageButton) findViewById(R.id.imBtn_infant_plus);

        tv_adult_total   = (TextView) findViewById(R.id.tv_adult_total);
        tv_child_total   = (TextView) findViewById(R.id.tv_child_total);
        tv_infant_total  = (TextView) findViewById(R.id.tv_infant_total);

        tv_adult_total.setText(""+adult_total);
        tv_child_total.setText(""+child_total);
        tv_infant_total.setText(""+infant_total);

        imBtn_adult_plu.setOnClickListener(btnPassenger);
        imBtn_adult_min.setOnClickListener(btnPassenger);
        imBtn_child_plu.setOnClickListener(btnPassenger);
        imBtn_child_min.setOnClickListener(btnPassenger);
        imBtn_infant_plu.setOnClickListener(btnPassenger);
        imBtn_infant_min.setOnClickListener(btnPassenger);

        //for seat class
        cb_economy      = (CheckBox)findViewById(R.id.cb_economy);
        cb_bussiness    = (CheckBox)findViewById(R.id.cb_business);
        cb_premium      = (CheckBox)findViewById(R.id.cb_premium);

        cb_economy.setOnCheckedChangeListener(new CBListenerSeatClass());
        cb_bussiness.setOnCheckedChangeListener(new CBListenerSeatClass());
        cb_premium.setOnCheckedChangeListener(new CBListenerSeatClass());

        ll_return_date.setVisibility(View.GONE);
        sw_round_trip = (Switch) findViewById(R.id.switchRoundTrip);
        sw_round_trip.setChecked(false);
        sw_round_trip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ll_passenger.animate()
                            .translationY(ll_return_date.getHeight())
                            .alpha(1.0f)
                            .setListener(null);
                    ll_seat_class.animate()
                            .translationY(ll_return_date.getHeight())
                            .alpha(1.0f)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    ll_return_date.setVisibility(View.VISIBLE);
                                    ll_passenger.setTranslationY(0);
                                    ll_seat_class.setTranslationY(0);
                                    ll_search.setTranslationY(0);
                                }
                            });
                    ll_search.animate()
                            .translationY(ll_return_date.getHeight())
                            .alpha(1.0f)
                            .setListener(null);
                    //display_message("true");
                }else {
                    ll_return_date.setVisibility(View.GONE);
                    ll_passenger.setTranslationY(ll_return_date.getHeight());
                    ll_seat_class.setTranslationY(ll_return_date.getHeight());
                    ll_search.setTranslationY(ll_return_date.getHeight());
                    ll_passenger.animate()
                            .translationY(0)
                            .alpha(1.0f)
                            .setListener(null);
                    ll_seat_class.animate()
                            .translationY(0)
                            .alpha(1.0f)
                            .setListener(null);
                    ll_search.animate()
                            .translationY(0)
                            .alpha(1.0f)
                            .setListener(null);
                }
            }
        });

        setDate();
        updateViewSeatClass();
    }
    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == imBtn_swap_origin){
                String temp = tv_origin.getText().toString();
                tv_origin.setText(tv_destination.getText().toString());
                tv_destination.setText(temp);
            }else if (v == ll_origin) {
                Intent i = new Intent(MainActivity.this, ChooseAirport.class);
                startActivity(i);
            }else if(v == ll_destination){
                Intent i = new Intent(MainActivity.this, ChooseAirport.class);
                startActivity(i);
            }else if(v == ll_depart_date){
                dateTimePick(tv_depart_date);
            }else if(v == ll_return_date){
                dateTimePick(tv_return_date);
            }else if(v == btn_search){
                Intent i = new Intent(MainActivity.this, ListFlight.class);
                startActivity(i);
            }else if(v == ll_seat_class){
                //showSeatClass();
            }
        }
    };
    View.OnClickListener btnPassenger = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == imBtn_adult_plu){
                adult_total++;
                updateView(tv_adult_total, ""+adult_total);
            }else if (v == imBtn_adult_min) {
                adult_total--;
                if(adult_total<=1) adult_total=1;
                updateView(tv_adult_total, ""+adult_total);
            }else if(v == imBtn_child_plu){
                child_total++;
                updateView(tv_child_total, ""+child_total);
            }else if(v == imBtn_child_min){
                child_total--;
                if(child_total<=0) child_total=0;
                updateView(tv_child_total, ""+child_total);
            }else if(v == imBtn_infant_plu){
                infant_total++;
                updateView(tv_infant_total, ""+infant_total);
            }else if(v == imBtn_infant_min){
                infant_total--;
                if(infant_total<=0) infant_total=0;
                updateView(tv_infant_total, ""+infant_total);
            }
        }
    };
    class CBListenerSeatClass implements CheckBox.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton cb, boolean isChecked) {
            if(cb==cb_economy) b_sc_economy = !b_sc_economy;
            if(cb==cb_bussiness)b_sc_bussiness = !b_sc_bussiness;
            if(cb==cb_premium)b_sc_premium = !b_sc_premium;

            if(!(b_sc_premium || b_sc_bussiness || b_sc_economy))
                b_sc_premium = true; cb_premium.setChecked(b_sc_premium);
            updateViewSeatClass();
        }
    }
    private void updateViewSeatClass(){
        String seat_class = "";
        if(b_sc_premium) seat_class+=getResources().getString(R.string.sflightdc_premium)+" ";
        if(b_sc_bussiness) seat_class+=getResources().getString(R.string.sflightdc_business)+" ";
        if(b_sc_economy) seat_class+=getResources().getString(R.string.sflightdc_economy)+" ";
        if(b_sc_premium && b_sc_bussiness && b_sc_economy){
            seat_class = getResources().getString(R.string.sflight_all);
        }
        tv_seat_class.setText(""+seat_class);
    }
    private void dateTimePick(final TextView textView){
        final View dialogView = View.inflate(this, R.layout.date_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        final DatePicker dp_date = (DatePicker) dialogView.findViewById(R.id.date_picker);
        final TextView t_title = (TextView) dialogView.findViewById(R.id.t_title_dialog);

        String s_title = "";
        if(textView == tv_depart_date){
            s_title = getResources().getString(R.string.sflightdt_depart_date);
            dp_date.setMinDate(System.currentTimeMillis());
            Log.d(getString(R.string.app_name), "depart date");
            dp_date.updateDate(c_depart.get(Calendar.YEAR), c_depart.get(Calendar.MONTH), c_depart.get(Calendar.DATE));
        }else {
            s_title = getResources().getString(R.string.sflightdt_return_date);
            Log.d(getString(R.string.app_name), ""+c_depart.DATE);
            dp_date.setMinDate(c_depart.getTimeInMillis());
            dp_date.updateDate(c_return.get(Calendar.YEAR), c_return.get(Calendar.MONTH), c_return.get(Calendar.DATE));
        }
        t_title.setText(s_title);

        dialogView.findViewById(R.id.btn_select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(getString(R.string.app_name), dp_date.getMinDate()+" ,in date");
                Calendar cal = new GregorianCalendar(dp_date.getYear(),
                        dp_date.getMonth(),
                        dp_date.getDayOfMonth());

                if(textView == tv_depart_date){
                    Log.d(getString(R.string.app_name), "depart date");
                    c_depart = cal;
                    if(c_return.before(c_depart)){
                        c_return = c_depart;
                    }
                }else{
                    c_return = cal;
                }
                updateDate();
                alertDialog.dismiss();
            }});

        dialogView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }});

        alertDialog.setView(dialogView);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.show();
    }
    private void setDate(){
        Log.i(getString(R.string.app_name), "Set tanggal awal.");
        c_depart = Calendar.getInstance();

        c_return = Calendar.getInstance();
        c_return.add(Calendar.DATE, 3);

        SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        tv_depart_date.setText(""+format_s.format(c_depart.getTime()));
        tv_return_date.setText(""+format_s.format(c_return.getTime()));
    }
    private void updateDate(){
        SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
        tv_depart_date.setText(""+format_s.format(c_depart.getTime()));
        tv_return_date.setText(""+format_s.format(c_return.getTime()));
    }
    private void showSeatClass(){
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        final View promptView = layoutInflater.inflate(R.layout.dialog_seat_class, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);
        final Button btn_cancel= (Button) promptView.findViewById(R.id.btn_cancel);
        final Button btn_select= (Button) promptView.findViewById(R.id.btn_select);

        final RadioGroup rg_seat_class = (RadioGroup)promptView.findViewById(R.id.rg_seat_class);
        ((RadioButton)rg_seat_class.getChildAt(0)).setChecked(true);

        final AlertDialog alert = alertDialogBuilder.create();
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alert.show();
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_seat_class.setText(((RadioButton) promptView.findViewById(rg_seat_class. getCheckedRadioButtonId())).getText().toString());
                alert.dismiss();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }

//    private void showPassenger(){
//        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
//        final View promptView = layoutInflater.inflate(R.layout.dialog_passenger, null);
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
//        alertDialogBuilder.setView(promptView);
//
//        final Button btn_cancel= (Button) promptView.findViewById(R.id.btn_cancel);
//        final Button btn_select= (Button) promptView.findViewById(R.id.btn_select);
//        final ImageButton imBtn_adult_min = (ImageButton) promptView.findViewById(R.id.imBtn_adult_minus);
//        final ImageButton imBtn_child_min = (ImageButton) promptView.findViewById(R.id.imBtn_child_minus);
//        final ImageButton imBtn_infant_min = (ImageButton) promptView.findViewById(R.id.imBtn_infant_minus);
//        final ImageButton imBtn_adult_plu = (ImageButton) promptView.findViewById(R.id.imBtn_adult_plus);
//        final ImageButton imBtn_child_plu = (ImageButton) promptView.findViewById(R.id.imBtn_child_plus);
//        final ImageButton imBtn_infant_plu = (ImageButton) promptView.findViewById(R.id.imBtn_infant_plus);
//
//        final TextView tv_adult_total   = (TextView) promptView.findViewById(R.id.tv_adult_total);
//        final TextView tv_child_total   = (TextView) promptView.findViewById(R.id.tv_child_total);
//        final TextView tv_infant_total  = (TextView) promptView.findViewById(R.id.tv_infant_total);
//
//        tv_adult_total.setText(""+adult_total);
//        tv_child_total.setText(""+child_total);
//        tv_infant_total.setText(""+infant_total);
//
//        View.OnClickListener btnPassenger = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(v == imBtn_adult_plu){
//                    adult_total++;
//                    updateView(tv_adult_total, ""+adult_total);
//                }else if (v == imBtn_adult_min) {
//                    adult_total--;
//                    if(adult_total<=0) adult_total=0;
//                    updateView(tv_adult_total, ""+adult_total);
//                }else if(v == imBtn_child_plu){
//                    child_total++;
//                    updateView(tv_child_total, ""+child_total);
//                }else if(v == imBtn_child_min){
//                    child_total--;
//                    if(child_total<=0) child_total=0;
//                    updateView(tv_child_total, ""+child_total);
//                }else if(v == imBtn_infant_plu){
//                    infant_total++;
//                    updateView(tv_infant_total, ""+infant_total);
//                }else if(v == imBtn_infant_min){
//                    infant_total--;
//                    if(infant_total<=0) infant_total=0;
//                    updateView(tv_infant_total, ""+infant_total);
//                }
//            }
//        };
//
//        imBtn_adult_plu.setOnClickListener(btnPassenger);
//        imBtn_adult_min.setOnClickListener(btnPassenger);
//        imBtn_child_plu.setOnClickListener(btnPassenger);
//        imBtn_child_min.setOnClickListener(btnPassenger);
//        imBtn_infant_plu.setOnClickListener(btnPassenger);
//        imBtn_infant_min.setOnClickListener(btnPassenger);
//
//        final AlertDialog alert = alertDialogBuilder.create();
//        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//        alert.show();
//        btn_select.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //tv_seat_class.setText(((RadioButton) promptView.findViewById(rg_seat_class. getCheckedRadioButtonId())).getText().toString());
//                alert.dismiss();
//            }
//        });
//        btn_cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alert.dismiss();
//            }
//        });
//    }
    private void updateView(TextView v, String s){
        v.setText(s);
        String passenger = adult_total+" "+getResources().getString(R.string.sflightdc_adult_value);
        if(child_total>0) passenger+=", "+child_total+ " "+getResources().getString(R.string.sflightdc_child_value);
        if(infant_total>0) passenger+=", "+infant_total+ " "+getResources().getString(R.string.sflightdc_infant_value);
        tv_passenger.setText(""+passenger);
    }
    private void display_message(String msg){
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
    }
}
