package com.ristekmuslim.travelristek.flight;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.ristekmuslim.travelristek.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imamudin on 22/08/17.
 */

public class ListFlight extends AppCompatActivity{

    private List<FlightModel> flightModels = new ArrayList<FlightModel>();
    private ListView listView;
    private FlightAdapter adapter;
    private SwipeRefreshLayout swipeContainer;

    LinearLayout ll_filter, ll_sort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flight_list);

        //untuk list view
        listView = (ListView)findViewById(R.id.custom_list);
        flightModels.clear();
        adapter = new FlightAdapter(ListFlight.this, flightModels);
        listView.setAdapter(adapter);
        getList();

        //untuk swipelist
        swipeContainer = (SwipeRefreshLayout)findViewById(R.id.swipeContainer);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));
        actionBar.setSubtitle("Jakarta - Surabaya");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        ll_filter   = (LinearLayout)findViewById(R.id.ll_filter);
        ll_sort     = (LinearLayout)findViewById(R.id.ll_sort);

        ll_sort.setOnClickListener(btnClick);
        ll_filter.setOnClickListener(btnClick);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
//                Object o = listView.getItemAtPosition(position);
//                Toast.makeText(ListFlight.this, "position: "+position,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ListFlight.this, FlightBooking.class);
                startActivity(intent);
            }
        });
    }
    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == ll_filter) {
                Intent a = new Intent(ListFlight.this, FilterFlight.class);
                startActivityForResult(a,100);
            }else if(v == ll_sort){
                showSortDialog();
            }
        }
    };
    private void showSortDialog(){
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        final View promptView = layoutInflater.inflate(R.layout.flight_sort_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ListFlight.this);
        alertDialogBuilder.setView(promptView);

        final AlertDialog alert = alertDialogBuilder.create();
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alert.setCancelable(true);
        alert.show();
    }
    private void getList(){
        for (int i = 0; i < 30; i++) {
            FlightModel fm = new FlightModel();

            flightModels.add(fm);
            adapter.notifyDataSetChanged();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }
}
