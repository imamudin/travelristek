package com.ristekmuslim.travelristek.flight;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ristekmuslim.travelristek.R;

import java.util.List;

/**
 * Created by imamudin on 22/08/17.
 */

public class FlightAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<FlightModel> piutangs;

    public FlightAdapter(Activity activity) {
        this.activity = activity;
    }
    public FlightAdapter(Activity activity, List<FlightModel> piutangs) {
        this.activity = activity;
        this.piutangs = piutangs;
    }

    @Override
    public int getCount() {
        return piutangs.size();
    }

    @Override
    public Object getItem(int location) {
        return piutangs.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.flight_item, null);

        TextView tv_detail   = (TextView)convertView.findViewById(R.id.tv_detail);
        tv_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(activity, "detail position : "+position, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(activity, FlightDetail.class);
                activity.startActivity(i);
            }
        });
        TextView tv_discount    = (TextView) convertView.findViewById(R.id.tv_discount_price);
        tv_discount.setPaintFlags(tv_discount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        TextView alamat_toko    = (TextView) convertView.findViewById(R.id.tv_alamat_toko);
//        TextView total_piutang  = (TextView) convertView.findViewById(R.id.tv_total_piutang);
//        TextView jumlah_faktur  = (TextView) convertView.findViewById(R.id.tv_jumlah_faktur);
//        TextView jatuh_tempo    = (TextView) convertView.findViewById(R.id.tv_jatuh_tempo);
//
//        Piutang m = piutangs.get(position);
//
//        nama_toko.setText(m.NAMA);
//        alamat_toko.setText(m.ALAMAT);
//        total_piutang.setText(decimalToRupiah(m.PIUTANG));
//        jumlah_faktur.setText(m.JUMLAH_FAKTUR);
//        jatuh_tempo.setText(m.TGL_JATUH_TEMPO);

        return convertView;
    }
}
