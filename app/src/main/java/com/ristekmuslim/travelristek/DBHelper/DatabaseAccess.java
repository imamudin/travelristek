package com.ristekmuslim.travelristek.DBHelper;

/**
 * Created by imamudin on 30/08/17.
 */

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import com.ristekmuslim.travelristek.airport.AirportModel;

import java.util.ArrayList;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    private static String T_AIRPORT     = "id_airports";
    private static String C_CODE        = "code";
    private static String C_NAME        = "name";
    private static String C_CITYCODE    = "cityCode";
    private static String C_CITYNAME    = "cityName";
    private static String C_COUNTRYNAME = "countryName";
    private static String C_COUNTRYCODE = "countryCode";
    private static String C_TIMEZONE    = "timezone";
    private static String C_LAT         = "lat";
    private static String C_LON         = "lon";
    private static String C_NUMAIRPORT  = "numAirports";
    private static String C_CITY        = "city";

    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public ArrayList<AirportModel> getAllAirports() {
        ArrayList<AirportModel> data = new ArrayList<AirportModel>();
        AirportModel one;
//        Cursor cursor = database.rawQuery("SELECT * FROM "+T_AIRPORT+" ORDER BY  "+C_CITYNAME+" ASC",null);
//        cursor.moveToFirst();
        String query ="SELECT * FROM "+T_AIRPORT+" ORDER BY  "+C_CITYNAME+" ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new AirportModel();
            one.setC_CODE(cursor.getString(0));
            one.setC_NAME(cursor.getString(1));
            one.setC_CITYCODE(cursor.getString(2));
            one.setC_CITYNAME(cursor.getString(3));
            one.setC_COUNTRYNAME(cursor.getString(4));
            one.setC_COUNTRYCODE(cursor.getString(5));
            data.add(one);
            cursor.moveToNext();
        }
        cursor.close();
        return data;
    }

    public ArrayList<AirportModel> getAirportsLike(String keyword) {
        ArrayList<AirportModel> data = new ArrayList<AirportModel>();
        AirportModel one;
        String query ="SELECT * FROM "+T_AIRPORT+" where "+C_CITYNAME+" like '%"+keyword+"%' or "+C_NAME+" like '%"+keyword+"%' ORDER BY  "+C_CITYNAME+" ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new AirportModel();
            one.setC_CODE(cursor.getString(0));
            one.setC_NAME(cursor.getString(1));
            one.setC_CITYCODE(cursor.getString(2));
            one.setC_CITYNAME(cursor.getString(3));
            one.setC_COUNTRYNAME(cursor.getString(4));
            one.setC_COUNTRYCODE(cursor.getString(5));
            data.add(one);
            cursor.moveToNext();
        }
        cursor.close();
        return data;
    }

    private class DatabaseOpenHelper extends SQLiteAssetHelper {
        private static final String DATABASE_NAME = "id_airports.db";
        private static final int DATABASE_VERSION = 3;

        public DatabaseOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
    }
}