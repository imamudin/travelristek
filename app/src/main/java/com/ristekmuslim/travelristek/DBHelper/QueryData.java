package com.ristekmuslim.travelristek.DBHelper;

/**
 * Created by imamudin on 30/08/17.
 */

public class QueryData {
    private String arab;
    private String indo;

    public String getArab() {
        return arab;
    }

    public void setArab(String arab) {
        this.arab = arab;
    }

    public String getIndo() {
        return indo;
    }

    public void setIndo(String indo) {
        this.indo = indo;
    }
}
