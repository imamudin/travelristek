package com.ristekmuslim.travelristek;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ristekmuslim.travelristek.promo.SamplePromoAdapter;

import me.relex.circleindicator.CircleIndicator;

import static com.ristekmuslim.travelristek.R.id.ll_skrinning;
import static com.ristekmuslim.travelristek.R.id.ll_skrinning1;

public class MainActivity2 extends AppCompatActivity {

    LinearLayout ll_skrining1, ll_skrining2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager viewpager = (ViewPager) findViewById(R.id.viewpager);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewpager.setAdapter(new SamplePromoAdapter());
        indicator.setViewPager(viewpager);

        ll_skrining1    = (LinearLayout)findViewById(ll_skrinning);
        ll_skrining2    = (LinearLayout)findViewById(ll_skrinning1);

        ll_skrining1.setOnClickListener(btnClick);
        ll_skrining2.setOnClickListener(btnClick);

    }
    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == ll_skrining1) {
                Intent a = new Intent(MainActivity2.this, MainActivity.class);
                startActivity(a);
            }else if(v == ll_skrining2){
                Toast.makeText(getApplicationContext(),"tes tes",Toast.LENGTH_SHORT).show();
            }
        }
    };
}
